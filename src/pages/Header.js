import React from 'react';
import { Link } from 'react-router-dom';
import './Header.css';

let Header = () => <header>
    <div className="title"><h1>Clean the forest, prevent fire!</h1></div>
    <div className="logo"><img src={"images/fire_logo.jpg"} alt="company logo"/></div>
    <nav>
        <Link className="menuitem" to={"/news"}> Notícias </Link>
        <Link className="menuitem" to={"/tasks"}> Tarefas </Link>
        <Link className="menuitem" to={"/builder"}> Inserir Tarefa</Link>
    </nav>
</header>

export default Header;

