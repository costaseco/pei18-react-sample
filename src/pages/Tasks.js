import React from 'react';
import './Tasks.css';

let TaskDetails = ({task}) =>
    <div id="taskdetails">
        <h2>Detalhes da Tarefa</h2>
        <p>{task.title}</p>
        <p>Descrição: {task.description}</p>
        <p>Tipo: {task.type}</p>
        <p>Técnico: {task.tech}</p>
        <div id="taskdetailmap"></div>
    </div>

class TaskList extends React.Component {
    constructor(props) {
        super();
        this.state = {
            selected:-1
        }
    }
    render() {
        var details;
        if( this.state.selected === -1 )
            details = <p>Seleccione uma tarefa</p>;
        else
            details = <TaskDetails task={this.props.tasks[this.state.selected]}/>;

        return <section>
            <div id="tasklist">
                <h2>Lista de tarefas</h2>
                <ul>
                    {
                        this.props.tasks.map(
                            (t, i) =>
                                <li onClick={() => this.selectTask(i)}>
                                    <p>{t.description}</p>
                                    <p>{t.tech}</p>
                                </li>
                        )
                    }
                </ul>
            </div>
            {details}
        </section>;
    }
    selectTask(i) {
        this.setState({selected:i});
    }
}

export default TaskList;


