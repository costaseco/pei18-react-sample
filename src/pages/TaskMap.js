import React from 'react';
import './TaskMap.css';
import { withGoogleMap, GoogleMap } from "react-google-maps"

// For documentation about the googlemaps component see:
//
// https://github.com/tomchentw/react-google-maps
// and
// https://github.com/tomchentw/react-google-maps/pull/168

let MyMap = withGoogleMap((props) =>
    <GoogleMap
        defaultZoom={10}
        defaultCenter={{ lat: 38.7, lng: -9 }}
    >
    </GoogleMap>
)

let TaskBuilder = (props) => <section>
    <h1>Adicionar uma tarefa de limpeza</h1>
    <p>Seleccione uma área a limpar no mapa.</p>
    <MyMap
        googleMapURL="https://maps.googleapis.com/maps/api/js?key= AIzaSyA_PdgS_87I7ydiRhFLxyiBRrnhHmTNlO0&v=3.exp"
        loadingElement={<div style={{ height: `100%` }} />}
        containerElement={<div style={{ height: `300px` }} />}
        mapElement={<div style={{ height: `100%` }} />}
    />
    <form id="newtaskform" onSubmit={props.addTask}>
        <div>
            <label for="description">Observações:</label>
            <textarea id="description"
                      name="description"
                      cols="50" rows="5"
                      placeholder="Insira uma breve descrição da tarefa a executar">
        </textarea>
        </div>
        <div>
            <label for="tasktype">Tipo de tarefa:</label>
            <select id="tasktype" name="tasktype">
                <option value="bush">Limpeza de mato</option>
                <option value="trash">Limpeza de Lixos</option>
                <option value="trees">Corte de Árvores</option>
            </select>
        </div>
        <div>
            <button>Adicionar Tarefa</button>
        </div>
    </form>
</section>

export default TaskBuilder;

