import React from 'react';
import './Footer.css';
import { withGoogleMap, GoogleMap } from "react-google-maps"

let FooterMap = withGoogleMap((props) =>
    <GoogleMap
        defaultZoom={15}
        defaultCenter={{ lat: 38.6617255, lng: -9.2064357 }}
    >
    </GoogleMap>
)

let Footer = () => <footer>
    <div id="sitemap">
        <h4>Mapa do site</h4>
        <ul>
            <li>
                <a href="#taskbuilder">Task Builder</a>
            </li>
            <li>
                <a href="#taskbuilder">Task List</a>
            </li>
        </ul>
    </div>
    <div id="contacts">
        <h4>Contactos</h4>
        <ul>
            <li>
                Emergência Florestal - 117
            </li>
            <li>
                Assistência técnica - 212 121 212
            </li>
        </ul>
    </div>
    <div id="location">
        <h4>Localização</h4>
        <div id="location_map">
            <FooterMap
                googleMapURL="https://maps.googleapis.com/maps/api/js?key= AIzaSyA_PdgS_87I7ydiRhFLxyiBRrnhHmTNlO0&v=3.exp"
                loadingElement={<div style={{ height: `100%` }} />}
                containerElement={<div style={{ height: `150px` }} />}
                mapElement={<div style={{ height: `100%` }} />}
            />
        </div>

        <div id="address">
            <p>Alameda dos Álamos, 110, 2ºEsq.</p>
            <p>1130-112 Algures</p>
        </div>
    </div>
</footer>

export default Footer;



