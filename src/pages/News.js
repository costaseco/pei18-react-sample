import React from 'react';
import './News.css';

var news = [
    { text: "Primeira Notícia", pic: "images/news1.jpg"},
    { text: "Segunda Notícia", pic: "images/news2.jpg"},
    { text: "Terceira Notícia", pic: "images/news3.jpg"},
    { text: "QUarta Notícia", pic: "images/news3.jpg"},
]

let NewsSection = () => <section id="news">
    <ul>
        {
            news.map( (n,i) =>
                <li>
                    <div className={"newspic"} id={"pic"+i}>
                        <img src={n.pic} alt={""}/>
                    </div>
                    <p>{n.text}</p>
                </li>
            )
        }
    </ul>
</section>;

export default NewsSection;