import React, { Component } from 'react';
import './App.css';
import Header from './pages/Header';
import Footer from './pages/Footer';
import NewsSection from './pages/News';
import TaskBuilder from './pages/TaskMap';
import TaskList from './pages/Tasks';
import {BrowserRouter as Router, Route} from "react-router-dom";

var initialTasks = [
    {title: "Tarefa 1",
        tech: "Técnico 1",
        description: "Esta é a tarefa 1",
        type: "Limpeza de Mato",
    },
    {title: "Tarefa 2",
        tech: "Técnico 2",
        description: "Esta é a tarefa 2",
        type: "Limpeza de Mato",
    },
    {title: "Tarefa 3",
        tech: "Técnico 3",
        description: "Esta é a tarefa 3",
        type: "Limpeza de Mato",
    },
];


class App extends Component {
    constructor() {
        super();
        this.state = {
            tasks: []
        }
        this.addTask = this.addTask.bind(this);
    }
    componentDidMount() {
        this.setState({tasks:initialTasks})
    }
    addTask(e) {
        e.preventDefault();
        var newtasks = [...this.state.tasks, {title: "Bla", description:"BlaBla", tech:"233"}];
        this.setState({ tasks: newtasks});
    }
    render() {
        return (
            <Router>
            <div>
                <Header />
                <Route exact path={"/news"} component={NewsSection}/>
                <Route exact path={"/builder"} component={() => <TaskBuilder
                                                                 tasks={this.state.tasks}
                                                                 addTask={this.addTask}/>}/>
                <Route exact path={"/tasks"} component={() => <TaskList
                                                               tasks={this.state.tasks}/>}/>
                <Footer />
            </div>
            </Router>
        );
  }
}

export default App;


