# ReactJS code sample

This code sample is the support for the lecture on web development given by the author for the students of "Projecto de Engenharia e Inovação" at FCT NOVA.

## Issues

If you find improvements or defects on this code, feel free to submit an issue, or even better, to fork the repository and submit a pull request

